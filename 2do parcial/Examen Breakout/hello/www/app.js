var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");
var my_gradient = ctx.createLinearGradient(0, 0, 0, 170);
var radio = 10;
var x = canvas.width / 2;
var y = canvas.height - 30;
var dx = 2;
var dy = -2;
var jugadorH = 10;
var jugadorW = 75;
var jugadorX = (canvas.width - jugadorW) / 2;
var hileras = 5;
var columnas = 4;
var ladrilloW = 75;
var ladrilloH = 20;
var ladrilloP = 10;
var separacionY = 30;
var separacionX = 30;
var puntos = 0;
var vidas = 3;

var grid = [];
for (c = 0; c < columnas; c++) {
  grid[c] = [];
  for (r = 0; r < hileras; r++) {
    grid[c][r] = {
      x: 0,
      y: 0,
      vivo: 1
    };
  }
}

document.addEventListener("mousemove", mouseMoveHandler, false);
document.addEventListener("touchstart", touchHandler);
document.addEventListener("touchmove", touchHandler);

my_gradient.addColorStop(0, "red");
my_gradient.addColorStop(0.5, "green");
my_gradient.addColorStop(1, "blue");

function mouseMoveHandler(e) {
  var relativo = e.clientX - canvas.offsetLeft;
  if (relativo > 0 && relativo < canvas.width) {
    jugadorX = relativo - jugadorW / 2;
  }
}

function touchHandler(e) {
  if (e.touches) {
    jugadorX = e.touches[0].pageX - canvas.offsetLeft - jugadorW / 2;
    jugadorY = e.touches[0].pageY - canvas.offsetTop - jugadorH / 2;
    e.preventDefault();
  }
}

function colision() {
  for (c = 0; c < columnas; c++) {
    for (r = 0; r < hileras; r++) {
      var b = grid[c][r];
      if (b.vivo == 1) {
        if (x > b.x && x < b.x + ladrilloW && y > b.y && y < b.y + ladrilloH) {
          dy = -dy;
          b.vivo = 0;
          puntos++;
          dx += 0.5;
          dy += 0.5;
          if (puntos == hileras * columnas) {
            alert("¡GANASTE!");
            document.location.reload();
          }
        }
      }
    }
  }
}

function dibujarPelota() {
  ctx.beginPath();
  ctx.arc(x, y, radio, 0, Math.PI * 2);
  ctx.fillStyle = "#dd0000";
  ctx.fill();
  ctx.closePath();
}

function dibujarJugador() {
  ctx.beginPath();
  ctx.rect(jugadorX, canvas.height - jugadorH, jugadorW, jugadorH);
  ctx.fillStyle = "#5ac682";
  ctx.fill();
  ctx.closePath();
}

function dibujarLadrillos() {
  for (c = 0; c < columnas; c++) {
    for (r = 0; r < hileras; r++) {
      if (grid[c][r].vivo == 1) {
        var gridX = (r * (ladrilloW + ladrilloP)) + separacionX;
        var gridY = (c * (ladrilloH + ladrilloP)) + separacionY;
        grid[c][r].x = gridX;
        grid[c][r].y = gridY;
        ctx.beginPath();
        ctx.rect(gridX, gridY, ladrilloW, ladrilloH);
        ctx.fillStyle = my_gradient;
        ctx.fill();
        ctx.closePath();
      }
    }
  }
}


function drawpuntos() {
  ctx.font = "16px Roboto";
  ctx.fillStyle = "#212121";
  ctx.fillText("Puntos: " + puntos, 8, 20);
}

function drawvidas() {
  ctx.font = "16px Roboto";
  ctx.fillStyle = "#212121";
  ctx.fillText("Vidas: " + vidas, canvas.width - 65, 20);
}

function draw() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  dibujarLadrillos();
  dibujarPelota();
  dibujarJugador();
  drawpuntos();
  drawvidas();
  colision();

  if (x + dx > canvas.width - radio || x + dx < radio) {
    dx = -dx;
  }
  if (y + dy < radio) {
    dy = -dy;
  } else if (y + dy > canvas.height - radio) {
    if (x > jugadorX && x < jugadorX + jugadorW) {
      dy = -dy;
    } else {
      vidas--;
      if (!vidas) {
        alert("Fin del Juego");
        document.location.reload();
      } else {
        x = canvas.width / 2;
        y = canvas.height - 30;
        dx = 3;
        dy = -3;
        jugadorX = (canvas.width - jugadorW) / 2;
      }
    }
  }

  x += dx;
  y += dy;
  requestAnimationFrame(draw);
}
draw();

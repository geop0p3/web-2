var config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 1200 },
            debug: false
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

var game = new Phaser.Game(config);
var puntos = 0;
var subiendo = false;
var puntostexto;
var direccion = 200;
var direccion2 = 200;
var direccion3 = 200;
var direccion4 = 200;
var direccion5 = 200;
var direccion6 = 200;
var direccion7 = 200;
var direccion8 = 200;



function preload(){
  this.load.spritesheet('jugador', 'recursos/jugador.png',
{ frameWidth: 32, frameHeight: 48 });
  this.load.image('diamante', 'recursos/diamante.png');
  this.load.image('fondo', 'recursos/fondo.png');
  this.load.image('plataforma', 'recursos/plataforma.png');
  this.load.image('ladder', 'recursos/ladder.png')
  this.load.spritesheet('barril','recursos/barril.png',{frameWidth:78,frameHeight:51})
}

function create(){
  this.add.image(400,300,'fondo')
  platformas = this.physics.add.staticGroup();
  escaleras = this.physics.add.staticGroup();

    platformas.create(400, 568, 'plataforma').setScale(2).refreshBody();
    platformas.create(-820, 400, 'plataforma');
    platformas.create(1530, 250, 'plataforma');
    platformas.create(-860, 100, 'plataforma');
    escaleras.create(570,155,'ladder').setScale(0.105).refreshBody();
    escaleras.create(460,155,'ladder').setScale(0.105).refreshBody();
    escaleras.create(100,305,'ladder').setScale(0.105).refreshBody();
    escaleras.create(280,305,'ladder').setScale(0.105).refreshBody();
    escaleras.create(600,448,'ladder').setScale(0.105).refreshBody();

    jugador = this.physics.add.sprite(200, 450, 'jugador');
    barriles = this.physics.add.group();
    barril = barriles.create(20,20,'barril');
    barril2 =  barriles.create(-230,20,'barril');
    barril3 =  barriles.create(-530,20,'barril');
    barril4 =  barriles.create(-790,20,'barril');
    barril5 =  barriles.create(-1200,20,'barril');
    barril6 =  barriles.create(-1600,20,'barril');
    barril7 =  barriles.create(-1940,20,'barril');
    barril8 =  barriles.create(-2400,20,'barril');
    diamante = this.physics.add.group({
    key: 'diamante',
    repeat: 4,
    setXY: { x: 100, y: 0, stepX:20, stepY:110 }
});

platformas.scaleX = 2;
jugador.setCollideWorldBounds(true);
barril.setScale(0.5)
barril2.setScale(0.5)
barril3.setScale(0.5)
barril4.setScale(0.5)
barril5.setScale(0.5)
barril6.setScale(0.5)
barril7.setScale(0.5)
barril8.setScale(0.5)
cursors = this.input.keyboard.createCursorKeys();
this.physics.add.collider(diamante, platformas);
this.physics.add.overlap(jugador, diamante, punto, null, this);
this.physics.add.overlap(jugador, escaleras, subir, null, this);


this.anims.create({
    key: 'left',
    frames: this.anims.generateFrameNumbers('jugador', { start: 0, end: 3 }),
    frameRate: 10,
    repeat: -1
});

this.anims.create({
    key: 'turn',
    frames: [ { key: 'jugador', frame: 4 } ],
    frameRate: 20
});

this.anims.create({
    key: 'right',
    frames: this.anims.generateFrameNumbers('jugador', { start: 5, end: 8 }),
    frameRate: 10,
    repeat: -1
});

this.anims.create({
    key:'spin',
    frames: this.anims.generateFrameNumbers('barril', { start: 0, end: 3 }),
    frameRate: 10,
    repeat: -1
});
this.anims.create({
    key:'spinL',
    frames: this.anims.generateFrameNumbers('barril', { start: 3, end: 0 }),
    frameRate: 10,
    repeat: -1
});

collision = this.physics.add.collider(jugador, platformas);
this.physics.add.collider(barriles,platformas);
this.physics.add.overlap(jugador, barriles, gameover, null, this);
puntostexto = this.add.text(16, 16, 'Puntos: 0', { fontSize: '32px', fill: '#fafafa' });
}

function gameover(jugador,barril){
  this.scene.restart();
}

function subir (jugador,escaleras){
  console.log(jugador);
  if(cursors.up.isDown){
    collision.active =false;
    subiendo = true;
    jugador.setVelocityY(-150);
  }else if(cursors.down.isDown){
    collision.active =false;
    subiendo = true;
    jugador.setVelocityY(150);
  } else  {
    collision.active =true;
    subiendo= false;
  }
}

function punto(jugador,diamante){
  diamante.destroy();
  puntos += 200;
  puntostexto.setText('Puntos: ' + puntos);
}

function update(){
  if (cursors.left.isDown && subiendo == false)
{
    jugador.setVelocityX(-160);

    jugador.anims.play('left', true);
}
else if (cursors.right.isDown && subiendo == false)
{
    jugador.setVelocityX(160);

    jugador.anims.play('right', true);
}
else
{
    jugador.setVelocityX(0);

    jugador.anims.play('turn');
}

if (cursors.space.isDown && jugador.body.touching.down)
{
    jugador.setVelocityY(-345);
}
barril.setVelocityX(direccion);
barril2.setVelocityX(direccion2);
barril3.setVelocityX(direccion3);
barril4.setVelocityX(direccion4);
barril5.setVelocityX(direccion5);
barril6.setVelocityX(direccion6);
barril7.setVelocityX(direccion7);
barril8.setVelocityX(direccion8);
barril.anims.play('spin',true);
barril2.anims.play('spin',true);
barril3.anims.play('spin',true);
barril4.anims.play('spin',true);
barril5.anims.play('spin',true);
barril6.anims.play('spin',true);
barril7.anims.play('spin',true);
barril8.anims.play('spin',true);
if(barril.x > 680){
    direccion = -200;
}
if(barril.x < 10){
  direccion= 200;
}
if(barril.x <10 && barril.y >400){
  barril.x =20;
  barril.y=20;
}

if(barril2.x > 680){
    direccion2 = -200;
}
if(barril2.x < 10){
  direccion2= 200;
}
if(barril2.x <10 && barril2.y >400){
  barril2.x =20;
  barril2.y=20;
}

if(barril3.x > 680){
    direccion3 = -200;
}
if(barril3.x < 10){
  direccion3= 200;
}
if(barril3.x <10 && barril3.y >400){
  barril3.x =20;
  barril3.y=20;
}

if(barril4.x > 680){
    direccion4 = -200;
}
if(barril4.x < 10){
  direccion4= 200;
}
if(barril4.x <10 && barril4.y >400){
  barril4.x =20;
  barril4.y=20;
}

if(barril5.x > 680){
    direccion5 = -200;
}
if(barril5.x < 10){
  direccion5= 200;
}
if(barril5.x <10 && barril5.y >400){
  barril5.x =20;
  barril5.y=20;
}

if(barril6.x > 680){
    direccion6 = -200;
}
if(barril6.x < 10){
  direccion6= 200;
}
if(barril6.x <10 && barril6.y >400){
  barril6.x =20;
  barril6.y=20;
}

if(barril7.x > 680){
    direccion7 = -200;
}
if(barril7.x < 10){
  direccion7= 200;
}
if(barril7.x <10 && barril7.y >400){
  barril7.x =20;
  barril7.y=20;
}

if(barril8.x > 680){
    direccion8 = -200;
}
if(barril8.x < 10){
  direccion8= 200;
}
if(barril8.x <10 && barril8.y >400){
  barril8.x =20;
  barril8.y=20;
}

}

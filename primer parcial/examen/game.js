var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var WIDTH = canvas.width;
var HEIGHT = canvas.height;
var time = 20;
var direccion = {
  izquierda: false,
  derecha: false,
  arriba: false,
  abajo: false
};

var pacman = {
  x: 50,
  y: 50,
  size: 20,
  width: 20,
  height: 20,
  spdX: 0,
  spdY: 0
};

var arreglo = [];
var puntos = [];

var pared = {
  x: 0,
  y: 0,
  size: 20,
  width: 520,
  height: 20
};
var pared2 = {
  x: 0,
  y: 0,
  size: 20,
  width: 20,
  height: 520
};
var pared3 = {
  x: 500,
  y: 0,
  size: 20,
  width: 20,
  height: 520
};
var pared4 = {
  x: 0,
  y: 500,
  size: 20,
  width: 520,
  height: 20
};
var pared5 = {
  x: 130,
  y: 120,
  size: 20,
  width: 20,
  height: 150
};
var pared6 = {
  x: 180,
  y: 250,
  size: 20,
  width: 150,
  height: 20
};
var pared7 = {
  x: 370,
  y: 120,
  size: 20,
  width: 20,
  height: 150
};
var pared8 = {
  x: 245,
  y: 80,
  size: 20,
  width: 20,
  height: 180
};
var pared9 = {
  x: 70,
  y: 60,
  size: 20,
  width: 380,
  height: 20
};
var pared10 = {
  x: 420,
  y: 120,
  size: 20,
  width: 20,
  height: 150
};
var pared11 = {
  x: 80,
  y: 120,
  size: 20,
  width: 20,
  height: 150
};
var pared12 = {
  x: 80,
  y: 300,
  size: 20,
  width: 20,
  height: 150
};
var pared13 = {
  x: 80,
  y: 300,
  size: 20,
  width: 100,
  height: 20
};
var pared14 = {
  x: 320,
  y: 300,
  size: 20,
  width: 20,
  height: 150
};
var pared15 = {
  x: 140,
  y: 300,
  size: 20,
  width: 100,
  height: 20
};
var pared16 = {
  x: 285,
  y: 430,
  size: 20,
  width: 150,
  height: 20
};
var pared17 = {
  x: 185,
  y: 360,
  size: 20,
  width: 150,
  height: 20
};

var punto = {
  x: 105,
  y: 200,
  size: 20,
  width: 20,
  height: 20,
  active: true
};
var punto2 = {
  x: 105,
  y: 170,
  size: 20,
  width: 20,
  height: 20,
  active: true
};
var punto3 = {
  x: 105,
  y: 140,
  size: 20,
  width: 20,
  height: 20,
  active: true
};
var punto4 = {
  x: 105,
  y: 230,
  size: 20,
  width: 20,
  height: 20,
  active: true
};
var punto5 = {
  x: 395,
  y: 200,
  size: 20,
  width: 20,
  height: 20,
  active: true
};
var punto6 = {
  x: 395,
  y: 170,
  size: 20,
  width: 20,
  height: 20,
  active: true
};
var punto7 = {
  x: 395,
  y: 140,
  size: 20,
  width: 20,
  height: 20,
  active: true
};
var punto8 = {
  x: 395,
  y: 230,
  size: 20,
  width: 20,
  height: 20,
  active: true
};
var punto9 = {
  x: 110,
  y: 330,
  size: 20,
  width: 20,
  height: 20,
  active: true
};
var punto10 = {
  x: 140,
  y: 330,
  size: 20,
  width: 20,
  height: 20,
  active: true
};
var punto11 = {
  x: 170,
  y: 330,
  size: 20,
  width: 20,
  height: 20,
  active: true
};
var punto12 = {
  x: 200,
  y: 330,
  size: 20,
  width: 20,
  height: 20,
  active: true
};
var punto13 = {
  x: 110,
  y: 390,
  size: 20,
  width: 20,
  height: 20,
  active: true
};
var punto14 = {
  x: 110,
  y: 360,
  size: 20,
  width: 20,
  height: 20,
  active: true
};
var punto15 = {
  x: 110,
  y: 420,
  size: 20,
  width: 20,
  height: 20,
  active: true
};
var punto16 = {
  x: 290,
  y: 390,
  size: 20,
  width: 20,
  height: 20,
  active: false
};
var punto17 = {
  x: 30,
  y: 30,
  size: 20,
  width: 20,
  height: 20,
  active: false
};
var punto18 = {
  x: 30,
  y: 30,
  size: 20,
  width: 20,
  height: 20,
  active: false
};
moverpc = function() {
  for (i = 0; i < puntos.length; i++) {
    if (
      pacman.x < puntos[i].x + puntos[i].width &&
      pacman.x + pacman.width > puntos[i].x &&
      pacman.y < puntos[i].y + puntos[i].height &&
      pacman.y + pacman.height > puntos[i].y
    ) {
      puntuacion += 20;
      puntos[i].active = false;
      puntos[i].x = 800;
    }
  }

  for (i = 0; i < arreglo.length; i++) {
    if (
      pacman.x < arreglo[i].x + arreglo[i].width &&
      pacman.x + pacman.width > arreglo[i].x &&
      pacman.y < arreglo[i].y + arreglo[i].height &&
      pacman.y + pacman.height > arreglo[i].y
    ) {
      if (direccion.derecha) {
        colRight = true;
        colLeft = false;
        colUp = false;
        colDown = false;
        pacman.spdX = 0;
        pacman.x -= 2;
      } else if (direccion.abajo) {
        colDown = true;
        colLeft = false;
        colUp = false;
        colRight = false;
        pacman.spdY = 0;
        pacman.y -= 2;
      }
    }

    if (
      pacman.x + pacman.width > arreglo[i].x &&
      pacman.x < arreglo[i].x + arreglo[i].width &&
      pacman.y + pacman.height > arreglo[i].y &&
      pacman.y < arreglo[i].y + arreglo[i].height
    ) {
      if (direccion.izquierda) {
        colLeft = true;
        colRight = false;
        colUp = false;
        colDown = false;
        pacman.spdX = 0;
        pacman.x += 2;
      } else if (direccion.arriba) {
        colUp = true;
        colLeft = false;
        colRight = false;
        colDown = false;
        pacman.spdY = 0;
        pacman.y += 2;
      }
    }
  }

  if (colRight) {
  } else if (colDown) {
  } else {
    colRight = false;
    colDown = false;
    colLeft = false;
    colUp = false;
    pacman.x += pacman.spdX;
    pacman.y += pacman.spdY;
  }

  if (colLeft) {
  } else if (colUp) {
  } else {
    colLeft = false;
    colUp = false;
    colRight = false;
    colDown = false;
    pacman.x += pacman.spdX;
    pacman.y += pacman.spdY;
  }
};

var puntuacion = 0;
var game;
var colLeft = false;
var colRight = false;
var colUp = false;
var colDown = false;
var relojx;

var i = 0;

window.onload = function() {
  inicio();
};
document.addEventListener("keydown", function checkEvent(e) {
  if (e.keyCode == "87" && pacman.spdY != 1) {
    direccion.arriba = true;
    direccion.abajo = false;
    direccion.izquierda = false;
    direccion.derecha = false;
    pacman.spdY = -2;
    pacman.spdX = 0;
  }
  if (e.keyCode == "83" && pacman.spdY != -1) {
    direccion.abajo = true;
    direccion.arriba = false;
    direccion.izquierda = false;
    direccion.derecha = false;
    pacman.spdY = 2;
    pacman.spdX = 0;
  }
  if (e.keyCode == "65" && pacman.spdX != 1) {
    direccion.izquierda = true;
    direccion.derecha = false;
    direccion.arriba = false;
    direccion.abajo = false;
    pacman.spdX = -2;
    pacman.spdY = 0;
  }
  if (e.keyCode == "68" && pacman.spdX != -1) {
    direccion.derecha = true;
    direccion.izquierda = false;
    direccion.abajo = false;
    direccion.arriba = false;
    pacman.spdX = 2;
    pacman.spdY = 0;
  }
});

drawObject = function(obj) {
  ctx.beginPath();
  ctx.fillStyle = "rgb(255, 248, 73)";
  var rad = obj.size / 2;
  ctx.arc(obj.x - rad, obj.y - rad, 15, 0, 2 * Math.PI);
  ctx.fill();
  ctx.closePath();
};
drawpared = function(obj) {
  ctx.beginPath();
  ctx.strokeStyle = "rgb(25, 124, 187)";
  ctx.lineWidth = 7;
  var rad = obj.size / 2;
  ctx.rect(obj.x - rad, obj.y - rad, obj.width, obj.height);
  ctx.stroke();
  ctx.closePath();
};

drawpunto = function(obj) {
  if (obj.active) {
    ctx.beginPath();
    ctx.fillStyle = "#fafafa";
    var rad = obj.size / 2;
    ctx.rect(obj.x - rad, obj.y - rad, obj.width, obj.height);
    ctx.fill();
    ctx.closePath();
  }
};

function reloj() {
  var date = new Date();
  var h = date.getHours();
  var m = date.getMinutes();
  var s = date.getSeconds();
  var session = "AM";
  if (h == 0) {
    h = 12;
  }
  if (h > 12) {
    h = h - 12;
    session = "PM";
  }
  h = h < 10 ? "0" + h : h;
  m = m < 10 ? "0" + m : m;
  s = s < 10 ? "0" + s : s;
  relojx = h + ":" + m + ":" + s + " " + session;
  setTimeout(reloj, 1000);
}

draw = function() {
  ctx.beginPath();
  ctx.fillStyle = "#212121";
  ctx.rect(0, 0, 500, 500);
  ctx.fill();
  ctx.closePath();
  moverpc();
  drawObject(pacman);
  for (i = 0; i < arreglo.length; i++) {
    drawpared(arreglo[i]);
  }
  for (i = 0; i < puntos.length; i++) {
    drawpunto(puntos[i]);
  }
  drawpunto(punto17);
  ctx.fillStyle = "white";
  ctx.font = "bold 16px Roboto";
  ctx.fillText("Puntos: " + puntuacion, 150, 475, 500);
  ctx.fillText(relojx, 260, 475, 200);
};

iniciar = function() {
  arreglo[0] = pared;
  arreglo[1] = pared2;
  arreglo[2] = pared3;
  arreglo[3] = pared4;
  arreglo[4] = pared5;
  arreglo[5] = pared6;
  arreglo[6] = pared7;
  arreglo[7] = pared8;
  arreglo[8] = pared9;
  arreglo[9] = pared10;
  arreglo[10] = pared11;
  arreglo[11] = pared12;
  arreglo[12] = pared13;
  arreglo[13] = pared14;
  arreglo[14] = pared15;
  arreglo[15] = pared16;
  arreglo[16] = pared17;
  puntos[0] = punto;
  puntos[1] = punto2;
  puntos[2] = punto3;
  puntos[3] = punto4;
  puntos[4] = punto5;
  puntos[5] = punto6;
  puntos[6] = punto7;
  puntos[7] = punto8;
  puntos[8] = punto9;
  puntos[9] = punto10;
  puntos[10] = punto11;
  puntos[11] = punto12;
  puntos[12] = punto13;
  puntos[13] = punto14;
  puntos[14] = punto15;
  puntos[15] = punto16;
};

inicio = function() {
  puntuacion = 0;
  canvas.width = canvas.width;
  iniciar();
  clearInterval(game);
  game = setInterval(draw, time);
};

reloj();
